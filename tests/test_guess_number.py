import guess_number.guess_number as guess_number
import unittest
from unittest import mock
import os
import os.path


class TestGuessNumber(unittest.TestCase):
    @mock.patch('random.randint')
    def test_random_randint_should_be_called_random_randint(self, random_randint):
        guess_number.guess_int(1, 10)

        random_randint.assert_called_once_with(1, 10)
    
    @mock.patch('random.uniform')
    def test_random_uniform_should_be_called_random_uniform(self, random_uniform):
        guess_number.guess_float(1, 10)

        random_uniform.assert_called_once_with(1, 10)

    @mock.patch('random.randint')
    @mock.patch('random.uniform')
    def test_random_should_be_called_once_in_random_randint_and_random_uniform(self, random_randint, random_uniform):
        guess_number.guess_int(1, 100)
        guess_number.guess_float(1, 100)

        random_randint.assert_called_once_with(1, 100)
        random_uniform.assert_called_once_with(1, 100)

    @mock.patch('random.randint',return_value=int(15))
    def test_guess_int_give_7_and_50_should_be_return_15(self, random_randint):
        result = guess_number.guess_int(7, 50)
        self.assertEqual(result, 15, 'Should be 15')

        random_randint.assert_called_once()
        random_randint.guess_int.return_value = 9

    @mock.patch('random.randint',return_value=int(68))
    def test_guess_int_give_158_and_25_should_be_return_68(self, random_randint):
        result = guess_number.guess_int(158, 25)
        self.assertEqual(result, 68, 'Should be 68')

        random_randint.assert_called_once()
        random_randint.guess_int.return_value = 68   

    @mock.patch('random.randint',return_value=int(987))
    def test_guess_int_give_70_and_1876_should_be_return_68(self, random_randint):
        result = guess_number.guess_int(70, 1876)
        self.assertEqual(result, 987, 'Should be 987')

        random_randint.assert_called_once()
        random_randint.guess_int.return_value = 987

    @mock.patch('random.randint',return_value=int(88))
    def test_guess_int_give_88_and_88_should_be_return_88(self, random_randint):
        result = guess_number.guess_int(88,88)
        self.assertEqual(result, 88, 'Should be 88')

        random_randint.assert_called_once()
        random_randint.guess_int.return_value = 88

    @mock.patch('random.randint',return_value=int(-674))
    def test_guess_int_give_negative789_and_negative5_should_be_return_negative674(self, random_randint):
        result = guess_number.guess_int(-789, -5)
        self.assertEqual(result, -674, 'Should be -674')

        random_randint.assert_called_once()
        random_randint.guess_int.return_value = -674

    @mock.patch('random.randint',return_value=int(8))
    def test_guess_int_give_negative356_and_356_should_be_return_8(self, random_randint):
        result = guess_number.guess_int(-356, 356)
        self.assertEqual(result, 8, 'Should be 8')

        random_randint.assert_called_once()
        random_randint.guess_int.return_value = 8

    @mock.patch('random.uniform',return_value=float(55.8))
    def test_guess_float_give_10_to_100_should_be_return_55point8(self, random_uniform):
        result = guess_number.guess_float(10, 100)
        self.assertEqual(result, 55.8, 'Should be 55.8')

        random_uniform.assert_called_once()
        random_uniform.guess_float.return_value = 55.8
    
    @mock.patch('random.uniform',return_value=float(68.9))
    def test_guess_int_give_158_and_25_should_be_return_68point9(self, random_uniform):
        result = guess_number.guess_float(158, 25)
        self.assertEqual(result, 68.9, 'Should be 68.9')

        random_uniform.assert_called_once()
        random_uniform.guess_float.return_value = 68.9   

    @mock.patch('random.uniform',return_value=float(987.897))
    def test_guess_int_give_70_and_1876_should_be_return_987point897(self, random_uniform):
        result = guess_number.guess_float(70, 1876)
        self.assertEqual(result, 987.897, 'Should be 987.897')

        random_uniform.assert_called_once()
        random_uniform.guess_float.return_value = 987.897

    @mock.patch('random.uniform',return_value=float(88.88))
    def test_guess_int_give_88_and_88_should_be_return_88point88(self, random_uniform):
        result = guess_number.guess_float(88,88)
        self.assertEqual(result, 88.88, 'Should be 88.88')

        random_uniform.assert_called_once()
        random_uniform.guess_float.return_value = 88.88

    @mock.patch('random.uniform',return_value=float(-674.09))
    def test_guess_int_give_negative789_and_negative5_should_be_return_negative674point09(self, random_uniform):
        result = guess_number.guess_float(-789, -5)
        self.assertEqual(result, -674.09, 'Should be -674.09')

        random_uniform.assert_called_once()
        random_uniform.guess_float.return_value = -674.09

    @mock.patch('random.uniform',return_value=float(8.43267))
    def test_guess_int_give_negative356_and_356_should_be_return_8point43267(self, random_uniform):
        result = guess_number.guess_float(-356, 356)
        self.assertEqual(result, 8.43267, 'Should be 8.43267')

        random_uniform.assert_called_once()
        random_uniform.guess_float.return_value = 8.43267

